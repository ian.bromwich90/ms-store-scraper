"use strict";

// include modules
var fs = require('fs'),
	request = require('request'),
	cheerio = require('cheerio'),
	async 	= require('async'),
	accounting = require('accounting')


// variables
const locales = ['en-gb', 'de-de', 'en-us'];
const titles = [{
	title: 'Dovetail Games Euro-Fishing',
	id: 'C5828HS8K39D',
	url: 'https://www.microsoft.com/****/store/p/Dovetail-Games-Euro-Fishing/C5828HS8K39D'
},
{
	title: 'Silence',
	id: '9nblggh557rv',
	url: 'https://www.microsoft.com/****/store/p/silence-the-whispered-world-2/9nblggh557rv'
},
{
	title: 'Mafia III',
	id: 'bvv8lhvgpbs3',
	url: 'https://www.microsoft.com/****/store/p/mafia-iii/bvv8lhvgpbs3?rtc=1'
}
];

let data = {};


/**
 * initialise requests per title
 * @param  array of title objects
 */
function initRequest(titles) {


	// loop through each game
	// title
	async.map(titles, pageRequest, (e, r) => {
		
		// once each title is complete
		// write data to json file
		fs.writeFile('price.json', JSON.stringify(data), function(err) {
			console.log('Scraping complete');
        });
	});
}


/**
 * request pages per locales
 * @param  title object
 * @param  callback once all locale requests are complete
 */
function pageRequest(title, cb) {
	// create json node for 
	// title
	data[title.id] = [];


	// map each locale and async 
	// request product page
	async.map(locales, (locale, callback) => {

		let options = {
			headers: {
				'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
			}
		};

		// format URL with current
		// locale		
		options.url = title.url.replace('****', locale);

		// send ajax request
		request(options, function(error, response, html) {
			if (!error && response.statusCode == 200) {

				var productData = {};

				// access html with JQUERY
				// functions
				var $ = cheerio.load(html);

				// get data from srv_microdata
				productData.url 		= options.url;
				productData.price 		= Number($('.c-price .srv_microdata meta[itemprop="price"]').first().attr('content').replace(',','.'));
				productData.oldPrice 	= $('.c-price .srv_saleprice').first().text().replace(',','.')		// format, replace comma with dot (accounting js will manage this)
				productData.currency	= $('.c-price .srv_microdata meta[itemprop="priceCurrency"]').attr('content');
				productData.goldPrice 	= Number($('.cli_upsell-option.context-upsell-information .price-disclaimer span').first().text().replace(',','.').replace(/[^0-9\.]+/g,""));

				// format prices
				productData.oldPrice = Number(productData.oldPrice.replace(/[^0-9\.]+/g,"")); // remove price symbols

				// push to data to insert into json file
				data[title.id].push({locale: locale, currentPrice: productData.price, prevPrice: productData.oldPrice, url: productData.url, goldPrice: productData.goldPrice});

				// trigger async callback
				callback(false, true);
			}

			if(error || response.statusCode !== 200) {
				console.log('Error on ' + title.title);
			}
		});
	}, (e, r) => {
		// trigger callback
		cb(null, true);
	})
}


initRequest(titles);